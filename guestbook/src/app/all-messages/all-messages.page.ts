import { Component, OnInit } from '@angular/core';
import { WebserviceService } from '../webservice.service';
import { Message } from '../models/message';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-messages',
  templateUrl: './all-messages.page.html',
  styleUrls: ['./all-messages.page.scss'],
})
export class AllMessagesPage implements OnInit {

  messages;

  constructor(
    public webservice: WebserviceService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  edit(id) {
    this.router.navigateByUrl('/edit-message/' + id);
  }

  async ionViewDidEnter() {
    await this.webservice.getAllMessages()
      .then((messages: Message[]) => {
        this.messages = messages;
      });
  }

  async delete(id) {
    await this.webservice.deleteMessage(id)
      .then(() => {
        this.ionViewDidEnter();
      });
  }

}