import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'all-messages',
    pathMatch: 'full'
  },
  { path: 'new-message', loadChildren: './new-message/new-message.module#NewMessagePageModule' },
  { path: 'all-messages', loadChildren: './all-messages/all-messages.module#AllMessagesPageModule' },
  { path: 'edit-message/:id', loadChildren: './edit-message/edit-message.module#EditMessagePageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
